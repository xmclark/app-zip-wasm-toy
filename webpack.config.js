// webpack.config.js
const path = require('path');

module.exports = {
  entry: "./index.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "index.js",
  },
  mode: "development",
  devServer: {
    contentBase: path.join(__dirname, 'views'),
    watchContentBase: true,
    compress: true,
    port: 9000
  }
};