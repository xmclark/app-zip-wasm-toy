const x = import('./pkg/test_zip_wasm.js');

x.then(f => {
  console.log(f);
  var inputElement = document.getElementById('file');
  inputElement.addEventListener("change", function(){
    var fileList = this.files;
    var file = fileList[0];
    var reader = new FileReader();
    reader.onload = function(e) {
      var arrayBuffer = reader.result;
      var view = new Uint8Array(arrayBuffer);
      f.process_data(view);
    };
    reader.readAsArrayBuffer(file);
  }, false);
});