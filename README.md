app-zip-wasm-toy

Install the node dependencies
```
> npm install
```

Start the web server to hot reload javascript
```
> npm run serve
```

Build the wasm
```
> npm run build
```

Visit `localhost:9000` and open a file, check the console