extern crate wasm_bindgen;
extern crate js_sys;
extern crate zip;
extern crate web_sys;

use web_sys::console;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn process_data(data: &mut [u8]) {
    use std::io::Cursor;
    let d = &mut data.as_ref();
    let r = Cursor::new(d);
    let mut archive = zip::ZipArchive::new(r).unwrap();
    console::log_1(&JsValue::from("opening archive... ..."));
    console::log_1(&JsValue::from(format!("Length: {}", archive.len())));
    for i in 0..archive.len() {
        let file = archive.by_index(i).unwrap();
        let outpath = file.sanitized_name();
        if (&*file.name()).ends_with('/') {
            console::log_1(&JsValue::from(format!("File {} extracted to \"{}\"", i, outpath.as_path().display())));
        } else {
            console::log_1(&JsValue::from(format!("File {} extracted to \"{}\" ({} bytes)", i, outpath.as_path().display(), file.size())));
        }
    }
}
